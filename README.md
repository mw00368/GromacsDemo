# Gromacs Demo

Files accompanying Max's November 2022 gromacs crash course.

## DNA Demo

1. Building the DNA 

    - Open avogadro
    - Build > Insert > DNA/RNA
    - Click compass to navigate
    - File > SaveAs (dna_AVO.pdb)
    - Inspect in text editor

2. Preparing the PDB for Gromacs

    - launch python3 and go through prep.py
    - or just execture ./prep.py
    - Inspect result in text editor

3. Running pdb2gmx

    - login to EUREKA
    - (optionally) ssh into debug node
    - go through top_1.sh
    - or just execute as ./top_1.sh
    - inspect files

4. Solvating and neutralising the system

    - go through top_2.sh
    - or just execute as ./top_2.sh

5. Running minimisation

    - go through min.sh
    - or just execute as ./min.sh

6. Basic analysis

    - see graph created by min.sh

7. Visualisation

    - load trajectory in VMD
    - Graphics > Representations
    - Extensions > Analysis > RMSD Trajectory Tool

8. Python analysis

    - see dhb.py

