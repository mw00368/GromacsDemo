#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import asemolplot as amp

min_traj = amp.parsePDB("min/min_traj.pdb",index=":")

min_traj[0].summary(16)

N1N3_dist = []
N6O4_dist = []
torsion = []

for i,system in enumerate(min_traj):

	DA5 = system.residues[0]
	DT3 = system.residues[-1]

	DA5_N1 = DA5.get_atom("N1")
	DA5_N6 = DA5.get_atom("N6")
	DA5_N9 = DA5.get_atom("N9")
	DT3_N1 = DT3.get_atom("N1")
	DT3_C6 = DT3.get_atom("C6")
	DT3_O4 = DT3.get_atom("O4")
	DT3_N3 = DT3.get_atom("N3")

	if i == 0:
		DA5.summary()
		DT3.summary()

	N1N3_dist.append(np.linalg.norm(DT3_N3.np_pos - DA5_N1.np_pos))
	N6O4_dist.append(np.linalg.norm(DT3_O4.np_pos - DA5_N6.np_pos))

	# torsion
	normvec1 = np.cross(DA5_N6.np_pos - DA5_N1.np_pos,DA5_N9.np_pos - DA5_N1.np_pos)
	normvec2 = np.cross(DT3_O4.np_pos - DT3_N3.np_pos,DT3_N3.np_pos - DT3_C6.np_pos)
	torsion.append(np.arccos(np.clip(np.dot(normvec1/np.linalg.norm(normvec1),normvec2/np.linalg.norm(normvec2)),-1.0,1.0))/np.pi*180)

fig,ax = plt.subplots(nrows=2,figsize=[7,10])

ax[0].set_xlabel("Frame")
ax[0].set_ylabel("Donor-Acceptor Distance [Å]")
ax[0].plot(N1N3_dist,label='N1N3')
ax[0].plot(N6O4_dist,label='N6O4')
ax[0].legend()

ax[1].set_xlabel("Frame")
ax[1].set_ylabel("Torsion Angle [degrees]")
ax[1].plot(torsion)

fig.tight_layout(pad=1)

plt.show()
