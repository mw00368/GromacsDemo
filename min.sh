#!/bin/bash

#SBATCH --partition=debug_latest
#SBATCH --time=00-00:60:00
#SBATCH --job-name=dna_min_demo
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=28
#SBATCH --cpus-per-task=1
#SBATCH -o %j_%x.o
#SBATCH -e %j_%x.e
#SBATCH --constraint=op
#SBATCH --constraint=avx2
#SBATCH --constraint=avx512

source $GROMAX/load_gmx.sh
source $GROMAX/gmx_funcs.sh

# dirs
mkdir -p min
cd min

###### BIG COMMENT

# run md
gromax grompp -f ../mdp/min.mdp -c ../top/dna_rdy.gro -p ../top/topol.top -o min.tpr
gromax mdrun -v -deffnm min

# analysis
minimStats mdrun
gromax energy "Potential" -f "min.edr" -o "min_pot.xvg"
xvg2png -f min_pot -o min_pot -xl 'MD step' -yl 'Potential Energy kcal/mol' -ys -xt 5000

# # trajectories
gromax trjconv "DNA" -s "min.tpr" -f "min.trr" -o "min_traj.pdb"
gromax trjconv "DNA" -s "min.tpr" -f "min.trr" -o "min_fin.pdb" -dump 999
