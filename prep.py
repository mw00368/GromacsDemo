#!/usr/bin/env python3

import asemolplot as amp
system = amp.parsePDB("dna_AVO.pdb","DNA")
system.summary()
system.summary(16)
system.autoname_chains()
system.get_chain('X').residues
res = system.chains[0].residues[0]
res.summary()
amp.dna.prep4gmx(system)
system.summary(16)
amp.write("dna_AMP.pdb",system)
