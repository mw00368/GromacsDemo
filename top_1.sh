#!/bin/bash

# https://gitlab.surrey.ac.uk/mw00368/GromacsOnEUREKA/-/wikis/Pre-Simulation/Generating-Topologies-for-an-Arbitrary-PDB

source load_gmx.sh

# gmx_mpi help pdb2gmx | less
# ls /opt/proprietary-apps/gromacs/2018/share/gromacs/top/

mkdir -p top
cd top

gmx_mpi pdb2gmx -f ../dna_AMP.pdb -o dna.gro -ff charmm36-mar2019 -water spce -ter

