#!/bin/bash

source $GROMAX/load_gmx.sh
source $GROMAX/gmx_funcs.sh

cd top

# print the charge information
totalCharge topol*X.itp "DNA: X"
totalCharge topol*X.itp "DNA: Y"

# create the unit cell
gromax editconf -f "dna.gro" -o "dna_box.gro" -c -d 1 -bt cubic

# solvate the system
gromax solvate -cp "dna_box.gro" -cs spc216.gro -o "dna_box_sol.gro" -p "topol.top"

# prepare for neutralisation
gromax grompp -f ../mdp/ions.mdp -c "dna_box_sol.gro" -p "topol.top" -o ions.tpr

# neutralise
gromax genion "SOL" -s ions.tpr -o "dna_rdy.gro" -p "topol.top" -pname NA -nname CL -neutral

exit $?
